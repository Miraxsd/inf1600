.global func_s

func_s:
	# nous allons commencer par la partie ((e*b)+g)/d
    flds e  #st(0)=e
    flds b  #st(1)=e et st(0)=b  
    fmulp   #st(1)=st(0)=e*b
    flds g  #st(0)=g    st(1)=e*b
    faddp   #st(0)=st(1)= (e*b)+g
    flds d  #st(0)= d    st(1)= (e*b)+g
    fdivrp  #st(1)=st(0)= ((e*b)+g)/d
    #fstps a #a=((e*b)+g)/d)    st(1)= ((e*b)+g)/d) 

    #nous allons poursuivre la deuxieme partie (g-f)/(d+e)

    flds g  #st(1)= ((e*b)+g)/d     st(0)=g
    flds f  #st(1)=g    st(0)=f
    fsubrp  #st(0)=st(1)= g-f
    #fstps g #g=g-f  st(1)= g-f
    flds d  #st(0)=d
    flds e  #st(0)=d    st(0)=d
    faddp   #st(0)=st(1)= d+e
    ##fstps d #d=d+e  st(1)=d+e
    #flds g  #st(0)=g-f
    #flds d  #st(1)=g-f     st(0)=d+e 
    fdivrp  #st(1)=st(0)=(g-f)/(d+e)
    ##flds a  #st(1)=((g-f)/(d+e))     st(0)=((e*b)+g)/d
    fmulp  #st(0)=st(1)=((g-f)/(d+e))*((e*b)+g)/d)
    fstps a #a=((g-f)/(d+e))*((e*b)+g)/d)   st(1)=((g-f)/(d+e))*((e*b)+g)/d)
    

	ret
